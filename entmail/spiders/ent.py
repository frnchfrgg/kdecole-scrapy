#    kdecole-scrapy
#    Copyright (C) 2019  Julien "_FrnchFrgg_" Rivaud
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import scrapy
import itertools, urllib.parse, json, pathlib, datetime, html
from email.utils import make_msgid
from urllib.parse import urljoin

def full_unescape(s):
    while True:
        old = s; s = html.unescape(s)
        if old == s: return s

FIELD_ID="ID_COMMUNICATION"
HISTORY_DIR=pathlib.Path("history")

class EntSpider(scrapy.Spider):
    name = 'ent'
    user_agent = 'Mozilla/5.0 (X11; Linux x86_64; rv:128.0) Gecko/20100101 Firefox/128.0'


    def start_requests(self):
        try:
            config = dict(json.load(open("config.json")))["ent"]
        except:
            raise RuntimeError("Wrong config structure: it should be a dict "
                               "with an 'ent' sub-dict")
        if "url" not in config:
            raise RuntimeError("Missing ENT base url in config")
        if "user" not in config:
            raise RuntimeError("Missing ENT user name in config")
        if "password" not in config:
            raise RuntimeError("Missing ENT password in config")
        if "birthday" not in config:
            raise RuntimeError("Missing birthday in config")
        self.config = config

        self.login_steps = {}
        yield scrapy.Request(
            urljoin(config["url"], "sg.do?PROC=MESSAGERIE"),
            dont_filter=True)

    def parse(self, response):
        def dostep(step, **data):
            self.login_steps[step] = response.text
            if len(self.login_steps) < 9 and response.xpath("//form"):
                return scrapy.FormRequest.from_response(
                    response,
                    formdata=data,
                    callback=self.parse
                )
            print(f"Error: not yet logged in ({', '.join(self.login_steps)})")
            for s, (name, form) in enumerate(self.login_steps.items()):
                pathlib.Path(f"debug_login_{s}_{name}").write_text(form)

        if response.css("#js_boite_reception li a"):
            yield from self.parse_list(response)
        elif response.xpath("//input[@value='LYON-AAA_enseignant']"):
            yield dostep("choice",
                         selection='LYON-AAA_enseignant')
        elif response.xpath("//input[@name='j_username']"):
            if response.xpath("//label[@for='password-input']"
                            "[contains(text(),'naissance')]"):
                yield dostep("birthday",
                             j_password=self.config["birthday"])
            else:
                yield dostep("login",
                             j_username=self.config["user"],
                             j_password=self.config["password"])
        else:
            yield dostep("redir")

    def parse_list(self, response):
        if self.login_steps:
            print(f"Login steps: {', '.join(self.login_steps)}.")
        for thread in response.css("#js_boite_reception li a"):
            req = response.follow(thread, callback=self.parse_thread)
            req.meta["thread_title"] = thread.css("::text").get().strip()
            yield req

    def parse_thread(self, response):
        thread_url = response.request.url
        thread_id = urllib.parse.parse_qs(
                    urllib.parse.urlsplit(thread_url).query
                ).get(FIELD_ID, 0)[0]
        thread_dir = HISTORY_DIR / thread_id
        thread_dir.mkdir(parents=True, exist_ok=True)
        messageid = None
        all_dest = response.xpath("//*[@id='toutDestinataires']/parent::*")
        all_dest_html = all_dest.get() or ''
        for css_to_remove in ["#toutDestinataires", "button"]:
            all_dest_html = all_dest_html.replace(
                    all_dest.css(css_to_remove).get() or '',
                    '')
        all_dest_html = full_unescape(all_dest_html)
        result = {}
        put_thread_away = self.config.get("put-thread-away", False)
        for nth, message in enumerate(response.xpath(
                "//*[starts-with(@id,'discussion_message')]")):
            fragment = f"#discussion_message{nth}"
            # figure out if message has already been seen/sent
            # and generate a message id for In-Reply-To
            previous_messageid = messageid
            msg_hist = thread_dir / str(nth)
            try:
                messageid = msg_hist.read_text()
            except FileNotFoundError:
                messageid = make_msgid()
                try:
                    msg_hist.write_text(messageid)
                except:
                    # There was a problem writing the messageid, probably
                    # because no space is left. Stop processing the thread,
                    # and do not put it in the bin so that we can pick it up
                    # later. Also remove any dangling files.
                    put_thread_away = False
                    try:
                        msg_hist.unlink()
                    except FileNotFoundError:
                        pass
                    break
            else:
                continue
            # There is a bug in Kdecole where sometimes multiple messages
            # have the same id. Check for it and warn.
            if message.attrib["id"] != fragment[1:]:
                print(f"WARN: in thread {thread_id} "
                      f"message #{nth} is {message.attrib['id']}")
            # scrap the message contents and attachments
            author = " ".join(
                    s.strip()
                    for s in message.xpath(
                        ".//button[contains(@class, 'infos-redacteur')]"
                        "//abbr/@title"
                        ).getall()
                    )
            try:
                msgtime = datetime.datetime.fromisoformat(
                        message.xpath(".//time/@datetime").get() )
            except:
                msgtime = (datetime.datetime.now() +
                           datetime.timedelta(seconds=nth))
            body = message.css("hr ~ *")
            content = "".join(body.getall())
            footer = body.css(".jumbofiles")
            attachments = []
            if footer:
                content = content.replace(footer.get(), '')
                for item in footer.css("ul li"):
                    link = item.css("a::attr(href)").get()
                    if link:
                        attachments.append(response.urljoin(link))
            for css_to_remove in ["#actionsParticipation"]:
                content = content.replace(
                        body.css(css_to_remove).get() or '',
                        '')
            # build an item with all the information
            # attachments will be downloaded by the item pipeline
            maybe_re = "Re: " if previous_messageid else ""
            result[nth] = { "thread_url": thread_url,
                    "thread_id": thread_id,
                    "subject": maybe_re + response.meta["thread_title"],
                    "messageid": messageid,
                    "in-reply-to": previous_messageid,
                    "msg_url": urllib.parse.urljoin(thread_url, fragment),
                    "author": full_unescape(author),
                    "all_dest": all_dest_html,
                    "content": content,
                    "attachments": attachments,
                    "time": msgtime.timestamp() }
        # send the whole thread as one item so the later pipeline can put the
        # mails in IMAP in thread order (or clients like thunderbird will fail
        # to consider them as the same thread).
        if result: yield result
        # put the thread in the bin to avoid processing it until a new message
        # comes and KDECOLE gets it out of the bin. It also ensures that we
        # process all new threads even if there are more than a page can fit.
        # DO NOT DO THAT if we stopped halfway due to write errors
        if put_thread_away:
            delete_button = response.xpath(
                    "//a[contains(@href, 'MASQUER_COMMUNICATION')]")
            if delete_button:
                yield response.follow(delete_button[0],
                                    callback=lambda *_: None)
