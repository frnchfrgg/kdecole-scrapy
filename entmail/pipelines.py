#    kdecole-scrapy
#    Copyright (C) 2019  Julien "_FrnchFrgg_" Rivaud
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import scrapy
from scrapy.pipelines.media import MediaPipeline
import json, re
import urllib.parse, mimetypes

import imaplib
import email
from email.message import Message, EmailMessage

class EntmailPipeline(MediaPipeline):
    def open_spider(self, spider):
        try:
            config = dict(json.load(open("config.json")))["imap"]
        except:
            raise RuntimeError("Wrong config structure: it should be a dict "
                               "with an 'imap' sub-dict")
        if "host" not in config:
            raise RuntimeError("Missing IMAP host in config")
        if "dest" not in config:
            raise RuntimeError("Missing IMAP dest (recipient) in config")
        config["crypt"] = config.get("crypt", "none").lower()
        config.setdefault("port", imaplib.IMAP4_SSL_PORT
                                  if config["crypt"] == "ssl"
                                  else imaplib.IMAP4_PORT)
        config.setdefault("user", None)
        config.setdefault("password", None)
        config.setdefault("sender", "no-reply-{}@kdecole.org")
        config.setdefault("maxsize", 50 * 1024 * 1024)
        config.setdefault("mailbox", "INBOX")
        self.config = config

        if config["crypt"] == "ssl":
            imap = imaplib.IMAP4_SSL(config["host"], config["port"])
        else:
            imap = imaplib.IMAP4(config["host"], config["port"])
            if config["crypt"] == "tls":
                imap.starttls()

        if config["user"] is not None:
            imap.login(config["user"], config["password"])

        self.imap = imap

        super().open_spider(spider)

    def close_spider(self, spider):
        try:
            self.imap.close()
        except Exception as e:
            pass
        # this does not exist in any parent class
        #super().close_spider(spider)

    def sendmail(self, time,
                 author, subject, body, attachments=(),
                 messageid=None, inreplyto=None):
        message = EmailMessage()

        safe_author = re.sub("\s", ".", author)
        safe_author = re.sub("[^-A-Za-z0-9.]", "", safe_author) or "null"
        sender = self.config["sender"].format(safe_author)
        author = author.replace('"', '')
        message["From"] = f'"{author}" <{sender}>'
        message["Subject"] = subject
        message["To"] = self.config["dest"]
        message['Date'] = email.utils.formatdate(time, localtime=True)
        if messageid: message["Message-ID"] = messageid
        if inreplyto:
            message["In-Reply-To"] = message["References"] = inreplyto

        message.set_content(body, subtype="html")

        for name, mtype, blob in attachments:
            main, sub = mtype.split('/', 1)
            message.add_attachment(blob,
                    maintype=main, subtype=sub, filename=name)

        self.imap.append(self.config["mailbox"], "",
                         imaplib.Time2Internaldate(time),
                         bytes(message))

    def get_media_requests(self, big_item, info):
        for item in big_item.values():
            for url in item["attachments"]:
                req = scrapy.Request(url)
                req.meta["source_item"] = item
                yield req

    def media_to_download(self, request, info, item):
        return None # Force download

    def media_failed(self, failure, request, info):
        return failure

    def _path_and_type(self, headers):
        # To find out the file name and mimetype, we use the header parsing
        # capabilities of email.message.Message. Set the correct headers first.
        parser_message = Message()
        try:
            CD = headers.getlist('Content-Disposition')[0]
        except IndexError:
            pass
        else:
            parser_message['content-disposition'] = CD.decode()
        try:
            MT = headers.getlist('Content-Type')[0]
        except IndexError:
            pass
        else:
            parser_message['content-type'] = MT.decode()
        # Get the file name, or default to the url basename
        if not (filename := parser_message.get_filename()):
            path = urllib.parse.urlsplit(request.url).path
            filename = next(filter(None, reversed(path.rsplit('/'))), "")
        # Get the mimetype. Try to guess a better type if the default
        # "text/plain" is returned.
        if (mimetype := parser_message.get_content_type()) == "text/plain":
            mimetype = mimetypes.guess_type(filename, strict=False)[0]
            if mimetype is None:
                mimetype = 'application/octet-stream'
        return (filename, mimetype)

    def file_path(self, request, response, info):
        path, _ = self._path_and_type(response.headers)
        return path

    def media_downloaded(self, response, request, info, item=None):
        filename, mimetype = self._path_and_type(response.headers)
        source_item = request.meta["source_item"]
        source_item.setdefault("files", []).append(
                (filename, mimetype, response.body) )
        return response

    def item_completed(self, results, big_item, info):
        for _, item in sorted(big_item.items()):
            content = f"""<html>
                <div
                    style="word-wrap: break-word;
                           overflow-wrap: break-word;
                           max-width: 35em;">
                        {item["content"]}
                </div>
                <a href="{item["msg_url"]}">Lire le message sur l'ENT</a>
                <div>{item["all_dest"]}</div>
                </html>"""

            attachments = []
            totalsize = len(content)
            def sizekey(uple):
                _, _, blob = uple
                return len(blob)
            files = sorted(item.get("files", ()), key=sizekey)
            for (name, mtype, blob) in files:
                totalsize += len(blob)
                if totalsize > self.config["maxsize"]: break
                attachments.append( (name, mtype, blob) )

            self.sendmail(time        = item["time"],
                          author      = item["author"],
                          subject     = item["subject"],
                          body        = content,
                          attachments = attachments,
                          messageid   = item["messageid"],
                          inreplyto   = item["in-reply-to"])

        return super().item_completed(results, big_item, info)
